# garuda-beautyline

This is a fork of Beautyline by sajjad606. And icons from Candy icons by EliverLara.

https://gitlab.com/garuda-linux/themes-and-settings/artwork/beautyline

<br>
How to clone this repository:

```
git clone https://gitlab.com/azul4/content/icons-and-themes/garuda-beautyline.git
```

<br>

Do not compile in a chroot.
